const express = require('express');
var cors = require('cors');
const bodyParser = require('body-parser');
const Sequelize = require('sequelize');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded());
app.use('/', express.static('static'));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

const sequelize = new Sequelize('c9', 'myosotis26', '', {
    dialect: "mysql",
    host: "localhost"
});

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });
  
const User = sequelize.define('users', {
   firstName: {
       type: Sequelize.STRING,
       allowNull: false
   }, 
   surname: {
        type: Sequelize.STRING,
        allowNull: false
   },
   email: {
       type: Sequelize.STRING,
       allowNull: false,
       validate: {
           isEmail: true
       }
   }, 
   password: {
       type: Sequelize.STRING,
       isInt: true,
       isLowercase: true,       
       isUppercase: true, 
       isMoreThanEightChars(value) {
           if(value.length < 8){
               throw new Error('Minimum 8 characters.')
           }
       },
       allowNull: false
   }
});

const Organisation= sequelize.define('organisations', {
     organisationName: {
         type: Sequelize.STRING,
         allowNull: false
     },
     domain: {
         type: Sequelize.STRING,
         allowNull: false
     },
     address: {
         type: Sequelize.STRING,
         allowNull: false
     },
     org_owner :{
         type: Sequelize.INTEGER,
         references: {
             model: User,
             key: 'id',
             deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
         }
         
     }
});

const Contact = sequelize.define('contacts', {
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    experienceLevel: {
        type: Sequelize.ENUM,
        values: ['Entry Level', 'Mid Level', 'Senior Level'],
        allowNull: false
    },
    linkedinAddress: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    contact_own: {
        type: Sequelize.INTEGER,
        references: {
            model: User,
            key: 'id',
            deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
        }
    },
    contact_org: {
        type: Sequelize.INTEGER,
        references: {
            model: Organisation,
            key: 'id',
            deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
        }
    }
    
})

const PhoneNumber = sequelize.define('phonenumbers', {
    phone_number: {
        type: Sequelize.CHAR(10),
        allowNull:true,
    },
    phone_type: {
        type: Sequelize.ENUM,
        values: ['Work', 'Home', 'Mobile', 'Other'],
        allowNull:false
    },
    phone_own: {
        type: Sequelize.INTEGER,
        references: {
            model: Contact,
            key: 'id',
            deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
        }
    }
});

const Activities = sequelize.define('activities', {
    activityName: {
        type: Sequelize.STRING,
        allowNull: false
    },
    date: {
        type: Sequelize.DATE, 
        allowNull: false
    },
    time: {
        type: Sequelize.TIME, 
        allowNull: false
    },
    duration: {
        type: Sequelize.INTEGER, 
        allowNull: false
    },
    organizer: {
        type: Sequelize.INTEGER, 
    references: {
            model: User,
            key: 'id',
            deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
        }
    }
});

sequelize.sync({force: true}).then(()=>{
    console.log('Databases create successfully')
})


//USERS

app.post('/users', (request, response) => {
    User.create(request.body).then((result) => {
        response.status(200).json(result)
    }).catch((err) => {
        response.status(500).send(err)
    })
});

app.get('/users', (request, response) => {
    User.findAll().then((results) => {
        response.status(200).send(results);
    });
});

app.get('/users/:id', (request, response) => {
    User.findById(request.params.id).then((result) => {
        if(result) {
            response.status(200).json(result)
        } else {
            response.status(404).send('resource not found')
        }
    }).catch((err) => {
        console.log(err)
        response.status(500).send('database error')
    })
});

app.put('/users/:id', (request, response) => {
    User.findById(request.params.id).then((user) => {
        if(user) {
            user.update(request.body).then((result) => {
                response.status(201).json(result)
            }).catch((err) => {
                console.log(err)
                response.status(500).send('database error')
            })
        } else {
            response.status(404).send('resource not found')
        }
    }).catch((err) => {
        console.log(err)
        response.status(500).send('database error')
    })
});

app.delete('/users/:id', (request, response) => {
    User.findById(request.params.id).then((user) => {
        if(user) {
            user.destroy().then((result) => {
                response.status(204).send(result)
            }).catch((err) => {
                console.log(err)
                response.status(500).send('database error')
            })
        } else {
            response.status(404).send('resource not found')
        }
    }).catch((err) => {
        console.log(err)
        response.status(500).send('database error')
    })
});

//CONTACTS

app.post('/contacts', (request, response) => {
    Contact.create(request.body).then((result) => {
        response.status(200).json(result)
    }).catch((err) => {
        response.status(500).send(err)
    })
});

app.get('/contacts', (request, response) => {
    Contact.findAll().then((results) => {
        response.status(200).send(results);
    });
});

app.get('/contacts/:id', (request, response) => {
    Contact.findById(request.params.id).then((result) => {
        if(result) {
            response.status(200).json(result)
        } else {
            response.status(404).send('resource not found')
        }
    }).catch((err) => {
        console.log(err)
        response.status(500).send('database error')
    })
});

app.put('/contacts/:id', (request, response) => {
    Contact.findById(request.params.id).then((contact) => {
        if(contact) {
            contact.update(request.body).then((result) => {
                response.status(201).json(result)
            }).catch((err) => {
                console.log(err)
                response.status(500).send('database error')
            })
        } else {
            response.status(404).send('resource not found')
        }
    }).catch((err) => {
        console.log(err)
        response.status(500).send('database error')
    })
});

app.delete('/contacts/:id', (request, response) => {
    Contact.findById(request.params.id).then((contact) => {
        if(contact) {
            contact.destroy().then((result) => {
                response.status(204).send(result)
            }).catch((err) => {
                console.log(err)
                response.status(500).send('database error')
            })
        } else {
            response.status(404).send('resource not found')
        }
    }).catch((err) => {
        console.log(err)
        response.status(500).send('database error')
    })
});


//ORGANISATIONS

app.post('/organisations', (request, response) => {
    Organisation.create(request.body).then((result) => {
        response.status(200).json(result)
    }).catch((err) => {
        response.status(500).send(err)
    })
});

app.get('/organisations', (request, response) => {
    Organisation.findAll().then((results) => {
        response.status(200).send(results);
    });
});

app.get('/organisations/:id', (request, response) => {
    Organisation.findById(request.params.id).then((result) => {
        if(result) {
            response.status(200).json(result)
        } else {
            response.status(404).send('resource not found')
        }
    }).catch((err) => {
        console.log(err)
        response.status(500).send('database error')
    })
});

app.put('/organisations/:id', (request, response) => {
    Organisation.findById(request.params.id).then((org) => {
        if(org) {
            org.update(request.body).then((result) => {
                response.status(201).json(result)
            }).catch((err) => {
                console.log(err)
                response.status(500).send('database error')
            })
        } else {
            response.status(404).send('resource not found')
        }
    }).catch((err) => {
        console.log(err)
        response.status(500).send('database error')
    })
});

app.delete('/organisations/:id', (request, response) => {
    Organisation.findById(request.params.id).then((org) => {
        if(org) {
            org.destroy().then((result) => {
                response.status(204).send(result)
            }).catch((err) => {
                console.log(err)
                response.status(500).send('database error')
            })
        } else {
            response.status(404).send('resource not found')
        }
    }).catch((err) => {
        console.log(err)
        response.status(500).send('database error')
    })
});

//ACTIVITIES

app.post('/activities', (request, response) => {
    Activities.create(request.body).then((result) => {
        response.status(200).json(result)
    }).catch((err) => {
        response.status(500).send(err)
    })
});

app.get('/activities', (request, response) => {
    Activities.findAll().then((results) => {
        response.status(200).send(results);
    });
});

app.get('/activities/:id', (request, response) => {
    Activities.findById(request.params.id).then((result) => {
        if(result) {
            response.status(200).json(result)
        } else {
            response.status(404).send('resource not found')
        }
    }).catch((err) => {
        console.log(err)
        response.status(500).send('database error')
    })
})

app.put('/activities/:id', (request, response) => {
    Activities.findById(request.params.id).then((act) => {
        if(act) {
            act.update(request.body).then((result) => {
                response.status(201).json(result)
            }).catch((err) => {
                console.log(err)
                response.status(500).send('database error')
            })
        } else {
            response.status(404).send('resource not found')
        }
    }).catch((err) => {
        console.log(err)
        response.status(500).send('database error')
    })
});

app.delete('/activities/:id', (request, response) => {
    Activities.findById(request.params.id).then((act) => {
        if(act) {
            act.destroy().then((result) => {
                response.status(204).send(result)
            }).catch((err) => {
                console.log(err)
                response.status(500).send('database error')
            })
        } else {
            response.status(404).send('resource not found')
        }
    }).catch((err) => {
        console.log(err)
        response.status(500).send('database error')
    })
});


app.listen(8080, ()=>{
    console.log('Server started on port 8080...');
})