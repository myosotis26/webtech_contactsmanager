$(document).ready(function(e) {

  $(".w3-bar-item w3-button menu-item2").one("click", function(e) {

    var sectionUrl = $(this).attr("data-url");
    var callMethod = "POST";
    var dataHtml = "<h1>New Content</h1>";
    /*You can add a loader function here;*/

    $.ajax({
      url: sectionUrl,
      type: callMethod,
      cache: false,
      data: {
        'html': dataHtml
      },
      dataType: 'html',
      success: function(htmlReturned) {
        $(".content--new2").html(htmlReturned);
        alert("This will only be executed once");
        /* Remove Loader */
      }
    });

  });
  
  $("#.w3-bar-item w3-button menu-item3").one("click", function(e) {

    var sectionUrl = $(this).attr("data-url");
    var callMethod = "POST";
    var dataHtml = "<h1>New Content</h1>";
    /*You can add a loader function here;*/

    $.ajax({
      url: sectionUrl,
      type: callMethod,
      cache: false,
      data: {
        'html': dataHtml
      },
      dataType: 'html',
      success: function(htmlReturned) {
        $(".content--new3").html(htmlReturned);
        alert("This will only be executed once");
        /* Remove Loader */
      }
    });

  });
  
  
  $("#.w3-bar-item w3-button menu-item4").one("click", function(e) {

    var sectionUrl = $(this).attr("data-url");
    var callMethod = "POST";
    var dataHtml = "<h1>New Content</h1>";
    /*You can add a loader function here;*/

    $.ajax({
      url: sectionUrl,
      type: callMethod,
      cache: false,
      data: {
        'html': dataHtml
      },
      dataType: 'html',
      success: function(htmlReturned) {
        $(".content--new4").html(htmlReturned);
        alert("This will only be executed once");
        /* Remove Loader */
      }
    });

  });
  
  $("#.w3-bar-item w3-button menu-item1").one("click", function(e) {

    var sectionUrl = $(this).attr("data-url");
    var callMethod = "POST";
    var dataHtml = "<h1>New Content</h1>";
    /*You can add a loader function here;*/

    $.ajax({
      url: sectionUrl,
      type: callMethod,
      cache: false,
      data: {
        'html': dataHtml
      },
      dataType: 'html',
      success: function(htmlReturned) {
        $(".content--new1").html(htmlReturned);
        alert("This will only be executed once");
        /* Remove Loader */
      }
    });

  });
  

});