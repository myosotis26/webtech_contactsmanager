//POSTMAN

//USERS POST

1.SELECT POST
2.JSON FILE
{
"firstName": "Dana",
"surname": "Cojocaru",
"email": "danielacojocaru269@gmail.com",
"password": "Cojocaru.1002"
},
{
"firstName": "Ale",
"surname": "Cirnaru",
"email": "alecirnaru@gmail.com",
"password": "Cirnaru.1001"
}
3.URL: https://webtech-contactsmanager-myosotis26.c9users.io/users
4.SEND

//USERS GET

1.SELECT GET
2.URL: https://webtech-contactsmanager-myosotis26.c9users.io/users
3.SEND

//USERS PUT

1.SELECT PUT
2.JSON FILE
{
"firstName": "Daniela",
"surname": "Cojocaru",
"email": "danielacojocaru269@gmail.com",
"password": "Cojocaru.1002"
}
3.URL: https://webtech-contactsmanager-myosotis26.c9users.io/users/1
4.SEND

//USERS DELETE

1.SELECT DELETE
2.URL: https://webtech-contactsmanager-myosotis26.c9users.io/users/2
4.SEND


//ORGANISATIONS POST

1.SELECT POST
2.JSON FILE
{
"organisationName": "DanaC",
"domain": "IT",
"address": "Slobozia",
"org_owner": 1
},
{
"organisationName": "AleC",
"domain": "IT",
"address": "Bucharest",
"org_owner": 2
}
3.URL: https://webtech-contactsmanager-myosotis26.c9users.io/organisations
4.SEND

//ORGANISATIONS GET

1.SELECT GET
2.URL: https://webtech-contactsmanager-myosotis26.c9users.io/organisations
3.SEND

//ORGANISATIONS PUT

1.SELECT PUT
2.JSON FILE
{
"organisationName": "DanielaC",
"domain": "IT",
"address": "Slobozia",
"org_owner": 1
}
3.URL: https://webtech-contactsmanager-myosotis26.c9users.io/organisations/1
4.SEND

//ORGANISATIONS DELETE

1.SELECT DELETE
2.URL: https://webtech-contactsmanager-myosotis26.c9users.io/organisations/2
4.SEND










