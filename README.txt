Descriere proiect TechWeb - Contacts Manager



	Aplicatiile de tip Contact Manager reprezinta solutia ideala pentru echipele cu un numar redus de membri si pentru firmele mici si mijlocii 
care au nevoie de o solutie simpla pentru gestionarea contactelor, care sa ii ajute la desfasurarea activitatilor de zi cu zi.
	Rolul aplicatiei va fi acela de a-i ajuta pe utilizatori sa stocheze si sa caute mai usor informatii despre diferite persoane, in functie 
de adresa de e-mail a acestora. 
	Aceasta permite utilizatorilor să transforme multitudinea de contacte intr-o agenda ordonata care sa contina toate adresele de interes 
prin sistematizarea contactelor din toate conturile acestora.
	Astfel, user-ul va reduce timpul pe care trebuie, in mod  normal, sa-l aloce cautarii contactelor si il va redistribui comunicarii efective
cu acestea.

Ea va putea fi folosita atat de utilizatorii basic, cat si de utilizatorii din mediul business, pentru a le fi mai usor sa tina o evidenta 
a angajatilor, partenerilor, s.a.m.d.

Functionalitatile aplicatiei:

Navigare usoara
Sortarea contactelor in functie de nume/ subiectul conversatiei/ companie/ departament
Adaugare/ editare de contacte
Sincronizare cu conturile de Facebook, LinkedIn
Activare/ dezactivare sincronizare
Adaugarea contactelor favorite intr-un grup
Trimiterea unui reminder in vederea zilelor de nastere, a intalnirilor de afaceri, etc.
